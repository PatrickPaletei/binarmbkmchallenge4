package id.ac.ukdw.challenge4.HomeScreen.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes")
data class NotesEntity (
    @PrimaryKey(autoGenerate = true)
    var id :Int = 0,
    @ColumnInfo(name = "judul")
    var judul:String,
    @ColumnInfo(name = "date")
    var date:String,
    @ColumnInfo(name = "notes")
    var notes:String
)