package id.ac.ukdw.challenge4.LoginRegister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import id.ac.ukdw.challenge4.HomeScreen.ContentActivity
import id.ac.ukdw.challenge4.R
import id.ac.ukdw.challenge4.databinding.ActivityLoginBinding
import id.ac.ukdw.challenge4.LoginRegister.helper.Constant
import id.ac.ukdw.challenge4.LoginRegister.helper.PrefHelper

class LoginActivity : AppCompatActivity() {
    private lateinit var binding:ActivityLoginBinding
    lateinit var prefHelper: PrefHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        prefHelper = PrefHelper(this)

        val (email,password) = login()
        val emailEdt = binding.edtEmail.text
        val passEdt = binding.edtPassword.text

        binding.loginBtn.setOnClickListener {
            if(emailEdt.toString().equals(email)&&passEdt.toString().equals(password)){
                prefHelper.put( Constant.PREF_IS_LOGIN, true)
                moveIntent()
            }

        }
        binding.registerBtn.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }
    private fun login():Pair<String?,String?>{
        val email = prefHelper.getString( Constant.PREF_EMAIL )
        val passowrd = prefHelper.getString( Constant.PREF_PASSWORD )
        val pair = Pair(email,passowrd)
        return pair
    }
    override fun onStart() {
        super.onStart()
        if (prefHelper.getBoolean( Constant.PREF_IS_LOGIN )) {
            moveIntent()
        }
    }
    fun moveIntent(){
        startActivity(Intent(this, ContentActivity::class.java))
    }
}