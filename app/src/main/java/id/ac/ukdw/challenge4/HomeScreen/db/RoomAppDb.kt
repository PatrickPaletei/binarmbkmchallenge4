package id.ac.ukdw.challenge4.HomeScreen.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [NotesEntity::class], version = 1)
abstract class RoomAppDb:RoomDatabase() {

    abstract fun Dao():Dao


}