package id.ac.ukdw.challenge4.HomeScreen.Content

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import id.ac.ukdw.challenge4.HomeScreen.db.NotesEntity
import id.ac.ukdw.challenge4.R
import kotlinx.android.synthetic.main.list_item.view.*

class NotesAdapter(private val allNotes: List<NotesEntity>) : RecyclerView.Adapter<NotesAdapter.ViewHolder>()  {

//    interface onItemClickListener{
//        fun onItemClick(position: Int)
//    }
    class ViewHolder (view: View):RecyclerView.ViewHolder(view){
//        val editItem =itemView.findViewById<Button>(R.id.btn_edit)
//        val deleteItem = itemView.findViewById<Button>(R.id.btn_hapus)
//
//        init {
//            itemView.setOnClickListener {
//                mListener.onItemClick(adapterPosition)
//            }
//        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvJudul.text = allNotes[position].judul
        holder.itemView.tvDate.text = allNotes[position].date
        holder.itemView.tvNotes.text = allNotes[position].notes
    }

    override fun getItemCount(): Int = allNotes.size



}
