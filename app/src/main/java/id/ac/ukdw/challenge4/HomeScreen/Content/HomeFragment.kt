package id.ac.ukdw.challenge4.HomeScreen.Content

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import id.ac.ukdw.challenge4.HomeScreen.Content.popupdialog.InputDialogFragment
import id.ac.ukdw.challenge4.HomeScreen.db.NotesEntity
import id.ac.ukdw.challenge4.HomeScreen.db.RoomAppDb
import id.ac.ukdw.challenge4.R
import id.ac.ukdw.challenge4.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private lateinit var adapter: NotesAdapter
    private lateinit var recyclerView: RecyclerView
    private var _binding:FragmentHomeBinding?=null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val database = Room.databaseBuilder(
            requireContext(), RoomAppDb::class.java,
            "notes_database"
        ).allowMainThreadQueries()
            .build()
//        database.Dao().insertNotes(NotesEntity(judul = "patrick", date = "10/10/2001", notes = "halo"))
        //        binding.textView2.text = allNotes.toString()
        var allNotes = database.Dao().getAllNotesInfo()

        //rcy view
        binding.rcyView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = NotesAdapter(allNotes)
        }
//        binding.addBtn.setOnClickListener{
//            var dialog = InputDialogFragment()
//
//            dialog.show(supp)
//        }

//        val layoutManager = LinearLayoutManager(context)
//        recyclerView = view.findViewById(R.id.rcyView)
//        recyclerView.layoutManager= layoutManager
//        recyclerView.setHasFixedSize(true)
//        adapter = NotesAdapter(allNotes,this)
//        recyclerView.adapter = adapter


    }


}