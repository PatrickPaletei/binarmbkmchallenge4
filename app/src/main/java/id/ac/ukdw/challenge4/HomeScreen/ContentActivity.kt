package id.ac.ukdw.challenge4.HomeScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import id.ac.ukdw.challenge4.HomeScreen.db.RoomAppDb
import id.ac.ukdw.challenge4.R
import id.ac.ukdw.challenge4.databinding.ActivityContentBinding
import id.ac.ukdw.challenge4.databinding.ActivityLoginBinding

class ContentActivity : AppCompatActivity() {
    private lateinit var binding: ActivityContentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



    }
}