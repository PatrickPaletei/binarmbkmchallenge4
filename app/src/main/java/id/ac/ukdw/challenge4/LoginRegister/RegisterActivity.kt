package id.ac.ukdw.challenge4.LoginRegister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import id.ac.ukdw.challenge4.R
import id.ac.ukdw.challenge4.databinding.ActivityLoginBinding
import id.ac.ukdw.challenge4.databinding.ActivityRegisterBinding
import id.ac.ukdw.challenge4.LoginRegister.helper.Constant
import id.ac.ukdw.challenge4.LoginRegister.helper.PrefHelper

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    lateinit var prefHelper: PrefHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        prefHelper = PrefHelper(this)

        binding.btnDaftar.setOnClickListener{
            val pass = binding.edtPassword.text.toString()
            val confirmPass = binding.edtConfirmPassword.text.toString()
            if (binding.edtUsername.text.isNotEmpty()&&binding.editEmail.text.isNotEmpty()&&binding.edtPassword.text.isNotEmpty()&&binding.edtConfirmPassword.text.isNotEmpty()){
                if (pass == confirmPass) {
                    saveUser(
                        binding.edtUsername.text.toString(),
                        binding.editEmail.text.toString(),
                        binding.edtPassword.text.toString()
                    )
                    moveToLogin()
                    Toast.makeText(this, "Berhasil Daftar !", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    private fun saveUser(username: String,email:String, password: String){
        prefHelper.put( Constant.PREF_USERNAME, username )
        prefHelper.put( Constant.PREF_EMAIL, email )
        prefHelper.put( Constant.PREF_PASSWORD, password )

    }
    private fun moveToLogin(){
        startActivity(Intent(this, LoginActivity::class.java))
    }


}