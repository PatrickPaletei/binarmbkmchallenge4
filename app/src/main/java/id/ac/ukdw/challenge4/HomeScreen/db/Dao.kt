package id.ac.ukdw.challenge4.HomeScreen.db

import androidx.room.*
import androidx.room.Dao

@Dao
interface Dao {
    @Query("SELECT * FROM notes")
    fun getAllNotesInfo():List<NotesEntity>

    @Insert
    fun insertNotes(notes:NotesEntity)
    @Delete
    fun deleteNotes(notes: NotesEntity)
    @Update
    fun updateNotes(notes: NotesEntity)
}